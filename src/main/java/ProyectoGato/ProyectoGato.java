package ProyectoGato;

import javax.swing.JFrame;

public class ProyectoGato {
    public static void main(String[] args) {
        JFrame f = new JFrame("ProyectoGato");
        PanelGato panel = new PanelGato();
        f.add(panel);
        f.setSize(500, 500);
        f.setLocation(100, 100);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);
    }
    
}
