package calculadora;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public final class Calculadora extends JFrame implements ActionListener {

    private final StringBuilder valorDisplay;
    private Operador operPrevio = null;
    private Display display;
    private int contOperando = 0;
    private double operando1;
    private boolean puntoPresionado = false;

    private enum Operador {
        MULTIPLICACION, DIVISION, MAS, MENOS, IGUAL;
    }

    public Calculadora() {
        super("Calculator");
        valorDisplay = new StringBuilder(32);
        initComponents();
        setSize(230, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void initComponents() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        display = new Display();
        panel.add(display);
        panel.add(new Keypad(this));
        this.add(panel);
    }

    private void numeroPresionado (String numero) {
        if (this.operPrevio == Operador.IGUAL) {
            operPrevio = null;
            clearDisplay();
            puntoPresionado = false;
        }

        valorDisplay.append(numero);
        display.setValue(valorDisplay.toString());
    }
    
    private void ClearPresionado() {
        operPrevio = null;
        operando1 = 0;
        clearDisplay();
        contOperando = 0;
    }

    private void clearDisplay() {
        valorDisplay.delete(0, valorDisplay.length());
        display.setValue("0");
    }

    private void calcular (Operador oper, double b) {
        switch (oper) {
            case MAS:
                operando1 += b;
                break;
            case MENOS:
                operando1 -= b;
                break;
            case MULTIPLICACION:
                operando1 *= b;
                break;
            case DIVISION:
                operando1 /= b;
                break;
        }
    }

    private void operadorPresionado(Operador oper) {

        double operando2 = valorDisplay.length() > 0 ? Double.parseDouble(valorDisplay.toString()) : operando1;
        if(oper == Operador.IGUAL){
            calcular(operPrevio, operando2);
            contOperando = 0;

        }else{
            contOperando++;
            if(contOperando > 1) calcular(oper, operando2);
            else operando1 = operando2;
            clearDisplay();
        }
        if (puntoPresionado) display.setValue("" + operando1);
        else display.setValue("" + (long) operando1);
        this.operPrevio = oper;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JButton src = (JButton) e.getSource();
        switch (src.getText()) {
            case "0":
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
                numeroPresionado(src.getText());
                break;
            case "=":
                operadorPresionado(Operador.IGUAL);
                break;
            case "+":
                operadorPresionado(Operador.MAS);
                break;
            case "-":
                operadorPresionado(Operador.MENOS);
                break;
            case "x":
                operadorPresionado(Operador.MULTIPLICACION);
                break;
            case "/":
                operadorPresionado(Operador.DIVISION);
                break;
            case ".":
                if (!puntoPresionado) {
                    puntoPresionado = true;
                    valorDisplay.append('.');
                }
                break;
            case "C":
                ClearPresionado();
                break;
        }
    }
}
